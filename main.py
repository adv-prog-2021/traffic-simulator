import pygame
from road import Node
import random
from road import Car
from road import Road

RED= (200, 0, 0 )

def  main():
    screen = pygame.display.set_mode((900,700))
    clock = pygame.time.Clock()

    n1=Node(str(1),(100,100))
    n2=Node(str(2),(400,100))
    n3=Node(str(3),(100,400))
    n4=Node(str(4),(400,400))
    n5=Node(str(5),(100,600))
    n6=Node(str(6),(400,600))
    n7=Node(str(7),(600,600))
    n8=Node(str(8),(600,400))
    n9=Node(str(9),(600,100))

    n1.next([n2,n3])
    n2.next([n1,n4,n9])
    n3.next([n5])
    n4.next([n2,n6,n3,n8])
    n5.next([n6])
    n6.next([n4,n7])
    n7.next([n8])
    n8.next([n7,n4])
    n9.next([n8])

    nodes = [n1,n2,n3,n4,n5,n6,n7,n8,n9]
    speed=[1,5,1,5,10,5,1,10,5,1,10,5,1,5,10,5,5]#speed on every road
    c=0
    car_list = []
    road_list = []
    for i in range(0,len(nodes)):
            for u in range(0,len(nodes[i].list)):
                road=Road(screen,nodes[i],nodes[i].list[u],speed[c])
                c+=1
                road_list.append(road)


    EXIT = False

    button = pygame.Rect(600, 100, 20, 20)

    while not EXIT:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                EXIT = True

            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    if button.collidepoint(event.pos):
                        car_list.append(Car(random.choice(nodes)))

        #Kill function...........
        while random.randint(0,30) == 1:
            if len(car_list)!=0:
                car_list.remove(random.choice(car_list))

        screen.fill((255, 255, 255))
        pygame.draw.rect(screen, RED, button)

        for node in nodes:
            pygame.draw.circle(screen, (0,0,0), node.pos, 20, 1)

        for road in road_list:
            road.create_road()
            road.update_weight(car_list)
            
        for car in car_list:
            car.update()
            screen.blit(car.image,car.pos)

        pygame.display.update()
        clock.tick(30)




if __name__ == '__main__':
    pygame.init()
    main()
    pygame.quit()

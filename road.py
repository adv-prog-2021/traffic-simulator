import pygame
from pygame.math import Vector2
import random

class Node():
    def __init__(self, id, pos):
        self.id = id
        self.pos = pos
        self.list=[]
        self.counter=0


    def next(self, list_of_nodes):
        self.list = list_of_nodes




class Car(pygame.sprite.Sprite):
    def __init__(self, current_node):
        super().__init__()
        self.image = pygame.image.load('digit.png').convert()
        self.rect = self.image.get_rect(center=current_node.pos)
        self.current_node = current_node
        self.velocity = Vector2(0,0)
        self.max_vel = 5
        self.pos = Vector2(current_node.pos)
        self.random_choice()


    def update(self):
        steering = Vector2(self.next_node.pos) - Vector2(self.pos)
        distance = int(steering.length()) # Distance to the target.

        if distance<=self.max_vel:
            self.current_node = self.next_node
            self.random_choice()

        if distance != 0: # To avoid getting normalization errors when the length of the vector is zero
            steering.normalize_ip()

        self.velocity = steering * self.max_vel
        self.pos += self.velocity
        self.rect.center = self.pos


    def random_choice(self):
        self.next_node = random.choice(self.current_node.list)


class Road(pygame.sprite.Sprite):
    def __init__(self,screen,start,end,speed):
        super().__init__()
        self.weight=0    #represente the number of visited car on this road
        self.start=start #type node
        self.end=end     #type node
        self.screen=screen
        self.steering = Vector2(self.end.pos) - Vector2(self.start.pos)
        self.speed=speed # represent the speed that the car should have on this road


    def create_road(self):
        pygame.draw.line(self.screen,"black",self.start.pos,self.end.pos,1)


    def update_weight(self,car_list):
        for car in car_list:
            steering_car=Vector2(car.next_node.pos)-Vector2(car.current_node.pos)
            # a node can be the start of multiple roads so to distinguish between roads and to know on which road
            # the car should drive we compare the steering of the car(steering_car) with the steering of the road(steering)
            if self.steering==steering_car and self.start.pos==car.rect.center:
                # when there is matching between the steering of the road and the steering of the car we know that this car
                # on this road so we can update the speed of this car(max_value) base on the speed of the road(self.speed)
                car.max_vel=self.speed
                self.weight+=1
                print("road :"+str(self.start.id)+","+str(self.end.id)+"  weight : "+str(self.weight))

